#
# Description: register VM into Ad DNs via nsupdate
#
begin
require "open3" 
def register_DNS(prov, vmname)
  #echo -e "server xx.xx.xx.xx\nprereq nxdomain titi.elec.eskom.co.za\nupdate add titi.elec.eskom.co.za 86400 A 10.10.10.10\nsend\nanswer" | nsupdate
  #rc=0 => OK
  #rc=1 => server not responding
  #rc=2 => prereq not verifyed
  $evm.root['ae_result']         = "error"
  @servernames.each do |srvname| 
    ip=prov.get_option(:ip_addr)
    domain=prov.get_option(:dns_suffixes)
    ns_update="server #{srvname}
    prereq nxdomain #{vmname}.#{domain}
    update add #{vmname}.#{domain} 86400 A #{ip}
    send
    answer
    "
    $evm.log(:info,"CC- updating #{vmname}.#{domain} DNs in #{srvname} with #{ns_update}")
    begin
      result, stderr, status=Open3.capture3("nsupdate <<< '#{ns_update}'")
    #result=`nsupdate <<< "#{ns_update}"`
      #status=$?
      $evm.log(:info,"CC- updating DNs result #{status.inspect} STDERRR=#{stderr} result=#{result}")

    case status.exitstatus
      when 0
        $evm.log(:info," CC- #{vmname}.#{domain} registered in DNS")
        $evm.root['ae_result']         = "ok"
        break
      when 1
        $evm.log(:info," CC- #{vmname}.#{domain} : #{srvname} ERROR: #{stderr}")
        next
      when 2
        $evm.log(:info," CC- #{vmname}.#{domain} already exists in DNS => Aborting")
        $evm.root['ae_result']         = "ok"
        break
      else
        $evm.log(:info," CC- #{vmname}.#{domain} exited ith code #{status.exitsatatus} => Aborting")
    end
=begin
    rescue Exception => e  
      next
=end
    end
  end
end
  
  $evm.log(:info, "CC- registering  #{$evm.root['miq_provision']} in DNS")

if $evm.root["vm"].nil?
  prov = $evm.root['miq_provision']
  vm=prov.destination
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 1 : Prov=#{prov}, vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
else
  vm = $evm.root["vm"]
  prov=vm.miq_provision
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 2 : Prov=#{prov},  vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
end
    vmname = vm.name.upcase rescue nil
  vmname||= prov.get_option(:vm_name)
  $evm.log(:info, "Found VM:<#{vmname}>")
  ident=prov.get_option(:sysprep_identification)
  @dns_domain=$evm.root["ad_domain"]
  @ad_password=$evm.root["ad_password"]
  @ad_user=$evm.root["ad_admin_user"]
  @base_dc=$evm.root["ad_base_dn"]
  @bindDN=$evm.root["ad_bind_dn"]
  @servernames = $evm.root["ad_servers"].split(",")
    @oudn=prov.get_option(:ad_ou)

      $evm.log(:info," CC- ident=#{ident} ad_user=#{@bindDN} ou=#{@oudn} base_dc=#{@base_dc} servers=#{@servernames}")
  
  register_DNS(prov, vmname)
#            $evm.root['ae_result']         = "retry"

  $evm.root['ae_retry_interval'] = 60.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "retry"
    $evm.root['ae_retry_interval'] = 30.seconds
    exit MIQ_OK
end  
