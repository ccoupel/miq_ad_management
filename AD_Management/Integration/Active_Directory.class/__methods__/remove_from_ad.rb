#
# Description: remove VM in AD
#
begin
    require 'rubygems'
    require 'net/ldap'
require 'manageiq-password'
  # call_ldap
  def ldap_connection(bind_dn, bind_password, servername)
    ldap_servername=servername.strip
    $evm.log(:info, "AD definition: ldap://#{bind_dn}@#{ldap_servername} ")
    # setup authentication to LDAP
    ldap = Net::LDAP.new :host => ldap_servername, :port => 389,
        :auth => {
        :method => :simple,
        :username => bind_dn,
        :password => bind_password
    }
    return ldap
  end
  
  def search_vm_dn(prov, vmname)
    computer_dn = nil
    result=nil
      @servernames.each do |srvname| 
        servername=srvname.strip
        ldap=ldap_connection(@bindDN, @ad_password, servername)
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{servername} ")

    # Search LDAP for computername
        $evm.log(:info, "Searching LDAP server: #{servername} base_dc: #{@base_dc} for computer: #{vmname}")
        filter = Net::LDAP::Filter.eq("cn", vmname)
        [@oudn, @base_dc].each do |basedc|
          result=ldap.search(:base => basedc, :filter => filter)
          $evm.log(:info,"    CC- search #{vmname} in #{basedc} result1= #{result}")
          break unless result.blank?
        end
        
        result.each {|entry| $evm.log(:info,"serch result=#{entry}");computer_dn = entry.dn } unless result.nil?
        break unless computer_dn.nil?
      end
    computer_dn
  end
  
  def remove_ldap(prov, vmname, computer_dn)
    result_dn=computer_dn

    $evm.log(:info, "vm #{vmname} found with dn: #{computer_dn}")
    @servernames.each do |srvname| 
        servername=srvname.strip   
        ldap=ldap_connection(@bindDN, @ad_password, servername)
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{servername} ")

      $evm.log(:info, "removing computer_dn from #{computer_dn} ")
      ldap.delete(:dn => computer_dn)
      result = ldap.get_operation_result
      if result.code.zero?
        $evm.log(:info, "Successfully removed computer_dn: #{computer_dn}")
        break
      else
        $evm.log(:warn, "Failed to move computer_dn: #{computer_dn} to #{new_dn}")
        $evm.log(:error, "Result Calling ldap:<#{result}>")
      end
    end
  end

 def call_ldap(prov, vmname)
   result=false
   computer_dn=search_vm_dn(prov, vmname)
   if computer_dn.nil?
     $evm.log(:info,"CC- #{vmname} not registered in AD : #{$evm.root["ae_state_retries"]}/#{$evm.root["ae_state_max_retries"]}")
   else
     new_dn=remove_ldap(prov,vmname, computer_dn)
   end
   return result
 end
  
  $evm.log(:info, "CC- removing LDAP #{$evm.root['vm']}")

  vmdb_object_type=$evm.root[$evm.root["vmdb_object_type"]]
  $evm.log(:info,"CC- vmdb_object_type=#{$evm.root["vmdb_object_type"]} : #{$evm.root["event_type"]}")
  $evm.log(:info,"CC- vmdb_object_type=#{vmdb_object_type.inspect}")

  case $evm.root["vmdb_object_type"]
    when "miq_provision"
      prov=vmdb_object_type
      vm=prov.destination
    when "vm"
      vm=vmdb_object_type
      prov=vm.miq_provision
    when "vm_retire_task"
      vm=vmdb_object_type.source
      prov=vm.miq_provision rescue nil
    when"event_stream"
      vm=vmdb_object_type.vm if $evm.root["event_type"]=="DestroyVM_Task_Complete"
      vm=vmdb_object_type.dest_vm if $evm.root["event_type"]=="CloneVM_Task_Complete"
      $evm.log(:info,"CC- DEST_VM=#{vm}")
      if vm.blank?
        $evm.root['ae_result']         = "retry"
        $evm.log(:info,"CC- DEST_VM not ready, retrying")
        exit MIQ_OK
      end
      prov=vm.miq_provision
  end  

    vmname = vm.name.upcase
  $evm.log(:info, "Found VM:<#{vmname}> with PROV=#{prov.id rescue "NONE"}")
  ident=prov.get_option(:sysprep_identification) rescue nil
  @dns_domain=$evm.root["ad_domain"]
  @ad_password=$evm.root["ad_password"]
  @ad_user=$evm.root["ad_admin_user"]
  @base_dc=$evm.root["ad_base_dn"]
  @bindDN=$evm.root["ad_bind_dn"]
  @servernames = $evm.root["ad_servers"].split(",")
    @oudn=prov.get_option(:ad_ou) rescue nil

      $evm.log(:info," CC- ident=#{ident} ad_user=#{@bindDN} ou=#{@oudn} base_dc=#{@base_dc} servers=#{@servernames}")
  
  result = call_ldap(prov, vmname) unless prov.blank? # if ident.downcase=="domain"
        
  $evm.root['ae_result']         = "ok" # exit if vmname found and moved
  $evm.root['ae_retry_interval'] = 60.seconds

rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 30.seconds
    exit MIQ_OK
end  
