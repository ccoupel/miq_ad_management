#
# Description: Wait until the VM is registered in AD and then move it to the requested ou
#
begin
    require 'rubygems'
    require 'net/ldap'
require 'manageiq-password'
  # call_ldap
  def ldap_connection(bind_dn, bind_password, servername)
    ldap_servername=servername.strip
    $evm.log(:info, "AD definition: ldap://#{bind_dn}@#{ldap_servername} ")
    # setup authentication to LDAP
    ldap = Net::LDAP.new :host => ldap_servername, :port => 389,
        :auth => {
        :method => :simple,
        :username => bind_dn,
        :password => bind_password
    }
    return ldap
  end
  
  def search_vm_dn(prov, vmname)
    computer_dn = nil
    result=nil
      @servernames.each do |srvname| 
        servername=srvname.strip
        ldap=ldap_connection(@bindDN, @ad_password, servername)
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{servername} ")

    # Search LDAP for computername
        $evm.log(:info, "Searching LDAP server: #{servername} base_dc: #{@base_dc} for computer: #{vmname}")
        filter = Net::LDAP::Filter.eq("cn", vmname)
        [@base_dc, "cn=computers,#{@base_dc}","#{@oudn}"].each do |basedc|
          result=ldap.search(:base => basedc, :filter => filter)
          $evm.log(:info,"    CC- search #{vmname} in #{basedc} result1= #{result}")
          break unless result.blank?
        end
        
        result.each {|entry| $evm.log(:info,"serch result=#{entry}");computer_dn = entry.dn } unless result.nil?
        break unless computer_dn.nil?
      end
    computer_dn
  end
  
  def move_ldap(prov, vmname, computer_dn)
    result_dn=computer_dn
      new_rdn="cn=#{vmname}"
      new_dn="#{new_rdn}, #{@oudn}"
    $evm.log(:info, "vm #{vmname} found with dn: #{computer_dn}")
    @servernames.each do |srvname| 
        servername=srvname.strip   
        ldap=ldap_connection(@bindDN, @ad_password, servername)
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{servername} ")

      $evm.log(:info, "moving computer_dn from #{computer_dn} to #{new_dn}")
      ldap.rename(:olddn => computer_dn, :newrdn =>new_rdn, delete_attributes: true, :new_superior => @oudn)
      result = ldap.get_operation_result
      if result.code.zero?
        $evm.log(:info, "Successfully moved computer_dn: #{computer_dn} to #{new_dn}")
        result_dn=new_dn
        break
      else
        $evm.log(:warn, "Failed to move computer_dn: #{computer_dn} to #{new_dn}")
        $evm.log(:error, "Result Calling ldap:<#{result}>")
        result_dn=computer_dn
        raise "Failed to move computer_dn: #{computer_dn} to #{new_dn}: Result Calling ldap:<#{result}>"
      end
    end
    result_dn
  end
  def add_to_group(prov, group_name, computer_dn)
    result_dn=computer_dn
    group_dn=search_vm_dn(prov, group_name) || search_vm_dn(prov, group_name.gsub("_","-")) 
    $evm.log(:info, "################group #{group_name} found with dn: #{group_dn}")
    @servernames.each do |servername|   
        ldap=ldap_connection(@bindDN, @ad_password, servername)
        $evm.log(:info, "AD definition: ldap://#{@bindDN}@#{servername} ")
      
      $evm.log(:info, "adding computer_dn  #{computer_dn} to #{group_dn}")
      ops=[[:add, :member, computer_dn]]
      ldap.modify(:dn => group_dn, :operations => ops)
      result = ldap.get_operation_result
      if result.code.zero?
        $evm.log(:info, "Successfully add computer_dn: #{computer_dn} to #{group_dn}")
        break
      else
        $evm.log(:warn, "Failed to add computer_dn: #{computer_dn} to #{group_dn}")
        $evm.log(:error, "Result Calling ldap:<#{result}>")
        raise "Failed to add computer_dn: #{computer_dn} to #{group_dn}: Result Calling ldap:<#{result}>"
      end
    end
  end  
 def call_ldap(prov, vmname)
   result=false
   computer_dn=search_vm_dn(prov, vmname)
   if computer_dn.nil?
     $evm.log(:info,"CC- #{vmname} not yet registered in AD : #{$evm.root["ae_state_retries"]}/#{$evm.root["ae_state_max_retries"]}")
   else
     new_dn=move_ldap(prov,vmname, computer_dn)
     unless new_dn.nil?
       group_name=$evm.root["miq_provision"].get_tag(:groupe_ad)
       result=add_to_group(prov, group_name, computer_dn) unless group_name.blank?
       $evm.log(:info,"CC- add group result=#{result}")
       result=true
     end
   end
   return result
 end
  
  $evm.log(:info, "CC- moving LDAP #{$evm.root['vm']}")

if $evm.root["vm"].nil?
  prov = $evm.root['miq_provision']
  vm=prov.destination
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 1 : Prov=#{prov}, vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
else
  vm = $evm.root["vm"]
  prov=vm.miq_provision
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 2 : Prov=#{prov},  vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
end
    vmname = vm.name.upcase
  $evm.log(:info, "Found VM:<#{vmname}>")
  ident=prov.get_option(:sysprep_identification)
  @dns_domain=$evm.root["ad_domain"]
  @ad_password=$evm.root["ad_password"]
  @ad_user=$evm.root["ad_admin_user"]
  @base_dc=$evm.root["ad_base_dn"]
  @bindDN=$evm.root["ad_bind_dn"]
  @servernames = $evm.root["ad_servers"].split(",")
    @oudn=prov.get_option(:ad_ou)

      $evm.log(:info," CC- ident=#{ident} ad_user=#{@bindDN} ou=#{@oudn} base_dc=#{@base_dc} servers=#{@servernames}")
  
  result = call_ldap(prov, vmname) if ident.downcase=="domain"
        
  $evm.root['ae_result']         = "ok" # exit if vmname found and moved
  $evm.root['ae_result']         = "retry"  if result==false # retry if vmname not found in AD
  $evm.root['ae_retry_interval'] = 60.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "error"
    $evm.root['ae_retry_interval'] = 30.seconds
    exit MIQ_OK
end  
