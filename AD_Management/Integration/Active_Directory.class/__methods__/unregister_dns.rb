#
# Description: register VM into Ad DNs via nsupdate
#

require "open3"
begin
  
def unregister_DNS(prov, vmname)
  #echo -e "server xx.xx.xx.xx\nprereq nxdomain titi.elec.eskom.co.za\nupdate add titi.elec.eskom.co.za 86400 A 10.10.10.10\nsend\nanswer" | nsupdate
  #rc=0 => OK
  #rc=1 => server not responding
  #rc=2 => prereq not verifyed
  @servernames.each do |srvname| 
    #ip=prov.get_option(:ip_addr)
    domain=prov.get_option(:dns_domain)
    ns_update="server #{srvname}
    prereq yxrrset #{vmname}.#{domain} A
    update delete #{vmname}.#{domain} A 
    send
    answer
    "
    $evm.log(:info,"CC- updating #{vmname}.#{domain} DNs in #{srvname} with #{ns_update}")
    begin
      #result=`nsupdate <<< "#{ns_update}"`
      #status=$?
      result, stderr, status = Open3.capture3("nsupdate <<< '#{ns_update}'")
      
      $evm.log(:info,"CC- updating DNs result #{status.inspect} STDERR=#{stderr} STDOUT=#{result}")

      case status.exitstatus
        when 0
          $evm.log(:info," CC- #{vmname}.#{domain} unregistered in DNS")
          break
        when 1
          $evm.log(:info," CC- #{vmname}.#{domain} : #{srvname} ERROR : #{stderr}")
          next
        when 2
          $evm.log(:info," CC- #{vmname}.#{domain} doesn't exists in DNS")
          break
        else
          raise(" CC- #{vmname}.#{domain} exited ith code #{status.exitsatatus} => Aborting")
      end
=begin
    rescue Exception  => e
      next
=end
    end

  end
end
  
  $evm.log(:info, "CC- unregistering  #{$evm.root['vm']} in DNS")

if $evm.root["vm"].nil?
  prov = $evm.root['miq_provision']
  vm=prov.destination
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 1 : Prov=#{prov}, vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
else
  vm = $evm.root["vm"]
  prov=vm.miq_provision
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "AD variables 2 : Prov=#{prov},  vmtargetname=#{vm}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
end
    vmname = vm.name.upcase
  $evm.log(:info, "Found VM:<#{vmname}>")

  unless prov.nil?
    ident=prov.get_option(:sysprep_identification)
    @dns_domain=$evm.root["ad_domain"]
    @ad_password=$evm.root["ad_password"]
    @ad_user=$evm.root["ad_admin_user"]
    @base_dc=$evm.root["ad_base_dn"]
    @bindDN=$evm.root["ad_bind_dn"]
    @servernames = $evm.root["ad_servers"].split(",")
    @oudn=prov.get_option(:ad_ou)

    $evm.log(:info," CC- ident=#{ident} ad_user=#{@bindDN} ou=#{@oudn} base_dc=#{@base_dc} servers=#{@servernames}")
  
    unregister_DNS(prov, vmname)
  else
    $evm.log(:info, "CC- #{vmname} with no miq_provision => exiting") if prov.nil?
  end

  $evm.root['ae_result']         = "ok"
  $evm.root['ae_retry_interval'] = 20.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "retry"
    $evm.root['ae_retry_interval'] = 30.seconds
    exit MIQ_OK
end  
